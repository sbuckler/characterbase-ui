import {Character} from "../types/character";

export class CharactersService {
    public async getCharacters(): Promise<Character[]> {
        const testCharacter = require("../test-character.json") as Character[];
        const response = await fetch("/api/characters");

        if (response.ok) {
            return await response.json();
        }

        testCharacter[0].id = Date.now().toString();

        return [testCharacter[0]];
    }

    public async createCharacter(character?: Character): Promise<Character> {
        const response = await fetch("/api/characters", {
            body: JSON.stringify(character),
            method: "POST"
        });

        if (response.ok) {
            return await response.json();
        }

        throw [response.status, response.statusText];
    }

    public async updateCharacter(character: Character): Promise<Character> {
        const response = await fetch("/api/characters", {
            body: JSON.stringify(character),
            method: "POST"
        });

        if (response.ok) {
            return await response.json();
        }

        throw [response.status, response.statusText];
    }

    public async deleteCharacter(character: Character): Promise<void> {
        const response = await fetch("/api/characters", {
            body: JSON.stringify(character),
            method: "DELETE"
        });

        if (response.ok) {
            return;
        }

        throw [response.status, response.statusText];
    }
}
