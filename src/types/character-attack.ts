export type DamageType = {
    id: string;
    name: "Bludgeoning" | "Piercing" | "Slashing";
};

export type CharacterAttack = {
    attackBonus: string;
    damage: string;
    damageType: DamageType;
    id: string;
    name: string;
    properties: string;
};
