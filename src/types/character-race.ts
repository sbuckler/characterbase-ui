export type CharacterRace = {
    id: string;
    name: string;
};
