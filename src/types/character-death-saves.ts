export type CharacterDeathSaves = {
    failures: number;
    successes: number;
};
