import {CharacterAlignment} from "./character-alignment";
import {CharacterClass} from "./character-class";
import {CharacterRace} from "./character-race";

export type CharacterDetails = {
    alignment: CharacterAlignment;
    background: string;
    class: CharacterClass[];
    experiencePoints: number;
    name: string;
    playerName: string;
    race: CharacterRace;
};
