import {DiceSides} from "./dice-sides";

export type CharacterHitDie = {
    die: Exclude<Exclude<DiceSides, "d20">, "d4">;
    remaining: number;
    total: number;
};

export type CharacterHitDice = CharacterHitDie[];
