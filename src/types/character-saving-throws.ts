import {CharacterAbilityScores} from "./character-ability-scores";
import {CharacterStatBonus} from "./character-stat-bonus";

export type CharacterSavingThrow = {
    bonuses: CharacterStatBonus[];
    proficient: boolean;
};

export type CharacterSavingThrows = {
    [key in keyof CharacterAbilityScores]: CharacterSavingThrow;
};
