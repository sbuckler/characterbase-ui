import {DiceSides} from "./dice-sides";

export type CharacterClass = {
    name: string;
    hitDie: Exclude<Exclude<DiceSides, "d20">, "d4">;
    id: string;
    levels: number;
};
