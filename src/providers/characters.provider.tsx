import * as React from "react";
import {CharactersRepository} from "../repositories/characters.repository";
import {Character} from "../types/character";

type State = {
    loading: boolean;
};

export class CharactersProvider<P = {}, S extends State = State> extends React.Component<P, S> {
    public state: S;
    public characters: Character[];
    private charactersRepository: CharactersRepository;

    constructor(props: P) {
        super(props, CharactersRepository);

        this.state = {
            loading: true
        } as S;

        this.charactersRepository = new CharactersRepository();
    }

    public async componentDidMount(): Promise<void> {
        await this.handleGetCharacters();
        this.setState(() => ({loading: false}));
    }

    protected handleGetCharacters = async (): Promise<void> => {
        this.characters = await this.charactersRepository.getCharacters();
    }

    protected handleAddCharacter = async (character: Character): Promise<void> => {
        try {
            await this.charactersRepository.addCharacter(character);
        } catch {
            await this.handleGetCharacters();
        }
    }

    protected handleCharacterUpdated = async (character: Character): Promise<void> => {
        await this.charactersRepository.updateCharacter(character);
        await this.handleGetCharacters();
    }

    protected handleCharacterRemoved = async (character: Character): Promise<void> => {
        await this.charactersRepository.removeCharacter(character);
        await this.handleGetCharacters();
    }
}
