import * as React from "react";
import * as ReactDOM from "react-dom";
import {App} from "./components/App/app.component";

ReactDOM.render(<App />, document.getElementById("appRoot"));
