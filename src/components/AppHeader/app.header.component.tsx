import {AppBar, Button, IconButton, Toolbar, Typography, withStyles, WithStyles} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import * as React from "react";
import {appHeaderStyles} from "./app-header.styles";

interface IProps extends WithStyles<typeof appHeaderStyles> {
    onNavMenuOpened: () => void;
    title: string;
}

class AppHeaderComponent extends React.Component<IProps> {

    constructor(props: IProps) {
        super(props);
    }

    public render(): JSX.Element {
        return (
            <AppBar className={this.props.classes.appHeader}
                    position="fixed">
                <Toolbar variant="dense">
                    <IconButton aria-label="Menu"
                                className={this.props.classes.appHeaderMenuButton}
                                color="inherit">
                        <MenuIcon onClick={this.props.onNavMenuOpened} />
                    </IconButton>

                    <Typography className={this.props.classes.appHeaderTitle}
                                color="inherit"
                                variant="h6">
                        {this.props.title}
                    </Typography>

                    <Button color="inherit">
                        Login
                    </Button>
                </Toolbar>
            </AppBar>
        );
    }
}

const appHeaderComponent = withStyles(appHeaderStyles)(AppHeaderComponent);

export {appHeaderComponent as AppHeaderComponent};
