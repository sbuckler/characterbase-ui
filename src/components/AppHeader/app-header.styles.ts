import {createStyles} from "@material-ui/core";
import {CharacterBaseTheme} from "../App/app.theme";

export const appHeaderStyles = (theme: CharacterBaseTheme) => createStyles({
    appHeader: {
        [theme.breakpoints.up("sm")]: {
            marginLeft: theme.appNav.width,
            width: `calc(100% - ${theme.appNav.width}px)`
        }
    },
    appHeaderMenuButton: {
        marginLeft: 0,
        marginRight: 20,
        [theme.breakpoints.up("sm")]: {
            display: "none"
        }
    },
    appHeaderTitle: {
        flexGrow: 1
    }
});
