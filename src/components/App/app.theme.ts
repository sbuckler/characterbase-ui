import {createMuiTheme, Theme} from "@material-ui/core";
import {Typography} from "@material-ui/core/styles/createTypography";

type CharacterBaseThemeTypography = Typography & {
    useNextVariants: boolean
};

export type CharacterBaseTheme = Theme & {
    appNav: {
        width: number;
    },
    typography: CharacterBaseThemeTypography
};

export const appTheme = createMuiTheme({
    appNav: {
        width: 360
    },
    overrides: {
        MuiFormControl: {
            root: {
                verticalAlign: ""
            }
        },
        MuiTypography: {
            overline: {
                fontWeight: "bold",
                lineHeight: 1.5
            }
        }
    },
    palette: {
        background: {
            default: "#fcfafa"
        },
        primary: {
            dark: "#261815",
            light: "#88564d",
            main: "#573731"
        },
        secondary: {
            dark: "#3f686f",
            light: "#8bb6bd",
            main: "#5b96a0"
        }
    },
    typography: {
        useNextVariants: true
    }
} as CharacterBaseTheme);
