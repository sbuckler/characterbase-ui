import {MuiThemeProvider, withStyles, WithStyles} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import * as React from "react";
import {AppBodyComponent} from "../AppBody/app.body.component";
import {AppHeaderComponent} from "../AppHeader/app.header.component";
import {AppNavComponent} from "../AppNav/app-nav.component";
import {appStyles} from "./app.styles";
import {appTheme} from "./app.theme";

interface IProps extends WithStyles<typeof appStyles> {
}

type State = {
    navIsOpen: boolean;
};

class App extends React.Component<IProps, State> {
    public state: State;

    constructor(props: IProps) {
        super(props);

        this.state = {
            navIsOpen: false
        };
    }

    public render(): JSX.Element {
        return (
            <MuiThemeProvider theme={appTheme}>
                <div className={this.props.classes.app}>
                    <CssBaseline />

                    <AppNavComponent navIsOpen={this.state.navIsOpen}
                                     onNavMenuOpened={this.toggleNavMenuOpen} />

                    <AppHeaderComponent onNavMenuOpened={this.toggleNavMenuOpen}
                                        title={"CharacterBase"} />

                    <AppBodyComponent />
                </div>
            </MuiThemeProvider>
        );
    }

    private toggleNavMenuOpen = (): void => {
        this.setState((prevState) => ({
            navIsOpen: !prevState.navIsOpen
        }));
    }
}

const app = withStyles(appStyles)(App);

export {app as App};
