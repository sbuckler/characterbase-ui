import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const characterSelectionStyles = (): StyleRules => createStyles({
    characterSelection: {
        padding: 8
    },
    characterSelectionField: {
        padding: 10
    }
});
