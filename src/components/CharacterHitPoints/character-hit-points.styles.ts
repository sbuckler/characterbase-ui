import {createStyles} from "@material-ui/core";

export const characterHitPointsStyles = createStyles({
    characterHitPoints: {
        padding: 8
    },
    characterHitPointsField: {
        padding: 10
    }
});
