import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterHitDice, CharacterHitDie} from "../../types/character-hit-dice";
import {CharacterHitDiceFieldComponent} from "./character-hit-dice-field.component";
import {characterHitDiceStyles} from "./character-hit-dice.styles";

type Props = WithStyles<typeof characterHitDiceStyles> & {
    onHitDiceUpdated: (hitDice: CharacterHitDice) => void;
    hitDice: CharacterHitDice;
};

class CharacterHitDiceComponent extends React.Component<Props> {
    public compileHitDiceFields = (): JSX.Element[] => {
        return this.props
            .hitDice
            .sort(this.sortHitDiceByDieSize)
            .map(this.mapHitDiceToJsx)
            .map(this.mapComponentToGridItem);
    }

    public handleHitDieUpdated = (updatedHitDie: CharacterHitDie): void => {
        const editedHitDie = this.props
            .hitDice
            .filter((hitDie: CharacterHitDie): boolean => hitDie.die === updatedHitDie.die)
            .map((hitDie: CharacterHitDie) => ({...hitDie, ...updatedHitDie}));

        const remainingHitDice = this.props
            .hitDice
            .filter((hitDie: CharacterHitDie): boolean => hitDie.die !== updatedHitDie.die);

        this.props.onHitDiceUpdated([...remainingHitDice, ...editedHitDie].sort(this.sortHitDiceByDieSize));
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterHitDice}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Hit Dice
                </Typography>

                <Grid container
                      direction="column">
                    {this.compileHitDiceFields()}
                </Grid>
            </Paper>
        );
    }

    private sortHitDiceByDieSize = (currentHitDie: CharacterHitDie, nextHitDie: CharacterHitDie): number => {
        const numberPattern = /\d+/;
        const currentHitDieMatch = numberPattern.exec(currentHitDie.die);
        const currentHitDieSize = currentHitDieMatch != null ? Number(currentHitDieMatch) : 0;
        const nextHitDieMatch = numberPattern.exec(nextHitDie.die);
        const nextHitDieSize = nextHitDieMatch != null ? Number(nextHitDieMatch) : 0;

        return currentHitDieSize - nextHitDieSize;
    }

    private mapHitDiceToJsx = (hitDie: CharacterHitDie): JSX.Element => {
        return <CharacterHitDiceFieldComponent hitDie={hitDie}
                                               onHitDieUpdated={this.handleHitDieUpdated} />;
    }

    private mapComponentToGridItem = (component: JSX.Element, index: number): JSX.Element => {
        return (
            <React.Fragment key={index}>
                {index > 0 && <Divider />}
                <Grid item
                      className={this.props.classes.characterHitDiceField}>
                    {component}
                </Grid>
            </React.Fragment>
        );
    }
}

const characterHitDiceComponent = withStyles(characterHitDiceStyles)(CharacterHitDiceComponent);

export {characterHitDiceComponent as CharacterHitDiceComponent};
