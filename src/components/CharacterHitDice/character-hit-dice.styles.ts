import {createStyles} from "@material-ui/core";

export const characterHitDiceStyles = createStyles({
    characterHitDice: {
        padding: 8
    },
    characterHitDiceField: {
        padding: 10
    }
});
