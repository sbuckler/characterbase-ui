import {FormControl, Grid, TextField} from "@material-ui/core";
import * as React from "react";
import {CharacterHitDie} from "../../types/character-hit-dice";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";
import {NumberStepperComponent} from "../NumberStepper/number.stepper.component";

type Props = {
    hitDie: CharacterHitDie;
    onHitDieUpdated: (hitDie: CharacterHitDie) => void;
};

export class CharacterHitDiceFieldComponent extends React.Component<Props> {
    public handleOnStep = (updatedRemaining: number): void => {
        this.props.onHitDieUpdated({...this.props.hitDie, remaining: updatedRemaining});
    }

    public render(): JSX.Element {
        return (
            <Grid container>
                <Grid item
                      xs={6}>
                    <CharacterTextFieldComponent currentValue={`${this.props.hitDie.total}${this.props.hitDie.die}`}
                                                 fullWidth={true}
                                                 label="Total"
                                                 readOnly={true} />
                </Grid>

                <Grid item
                      xs={6}>
                    <FormControl fullWidth={true}>
                        <TextField InputProps={{disableUnderline: true, readOnly: true}}
                                   label="Remaining"
                                   value={this.props.hitDie.remaining} />

                        <NumberStepperComponent value={this.props.hitDie.remaining}
                                                step={1}
                                                max={this.props.hitDie.total}
                                                min={0}
                                                onStepHandler={this.handleOnStep} />
                    </FormControl>
                </Grid>
            </Grid>
        );
    }
}
