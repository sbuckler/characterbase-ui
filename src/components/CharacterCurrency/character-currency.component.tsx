import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterCurrency} from "../../types/character-currency";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";
import {characterCurrencyStyles} from "./character-currency.styles";

type Props = WithStyles<typeof characterCurrencyStyles> & {
    currency: CharacterCurrency;
    onCurrencyUpdated: (currency: CharacterCurrency) => void;
};

class CharacterCurrencyComponent extends React.Component<Props> {
    public handleCopperUpdated = (amount: number): void => {
        this.props.onCurrencyUpdated({...this.props.currency, copper: amount});
    }

    public handleSilverUpdated = (amount: number): void => {
        this.props.onCurrencyUpdated({...this.props.currency, silver: amount});
    }

    public handleElectrumUpdated = (amount: number): void => {
        this.props.onCurrencyUpdated({...this.props.currency, electrum: amount});
    }

    public handleGoldUpdated = (amount: number): void => {
        this.props.onCurrencyUpdated({...this.props.currency, gold: amount});
    }

    public handlePlatinumUpdated = (amount: number): void => {
        this.props.onCurrencyUpdated({...this.props.currency, platinum: amount});
    }

    public render() {
        return (
            <Paper className={this.props.classes.characterCurrency}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Currency
                </Typography>

                <Grid container
                      direction="column">
                    <Grid className={this.props.classes.characterCurrencyField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.currency.copper}
                                                     fullWidth={true}
                                                     label="Copper Pieces"
                                                     min={0}
                                                     onChangeHandler={this.handleCopperUpdated}
                                                     readOnly={false} />
                    </Grid>

                    <Grid className={this.props.classes.characterCurrencyField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.currency.silver}
                                                     fullWidth={true}
                                                     label="Silver Pieces"
                                                     min={0}
                                                     onChangeHandler={this.handleSilverUpdated}
                                                     readOnly={false} />
                    </Grid>

                    <Grid className={this.props.classes.characterCurrencyField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.currency.electrum}
                                                     fullWidth={true}
                                                     label="Electrum Pieces"
                                                     min={0}
                                                     onChangeHandler={this.handleElectrumUpdated}
                                                     readOnly={false} />
                    </Grid>

                    <Grid className={this.props.classes.characterCurrencyField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.currency.gold}
                                                     fullWidth={true}
                                                     label="Gold Pieces"
                                                     min={0}
                                                     onChangeHandler={this.handleGoldUpdated}
                                                     readOnly={false} />
                    </Grid>

                    <Grid className={this.props.classes.characterCurrencyField}
                          item
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.currency.platinum}
                                                     fullWidth={true}
                                                     label="Platinum Pieces"
                                                     min={0}
                                                     onChangeHandler={this.handlePlatinumUpdated}
                                                     readOnly={false} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

const characterCurrencyComponent = withStyles(characterCurrencyStyles)(CharacterCurrencyComponent);

export {characterCurrencyComponent as CharacterCurrencyComponent};
