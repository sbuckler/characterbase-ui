import {createStyles} from "@material-ui/core";

export const characterCurrencyStyles = createStyles({
    characterCurrency: {
        padding: 8
    },
    characterCurrencyField: {
        padding: 10
    }
});
