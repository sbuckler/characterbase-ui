import * as React from "react";
import {CharacterClass} from "../../types/character-class";
import {CharacterOptionFieldComponent} from "../CharacterOptionField/character-option.field.component";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";

type Props = {
    currentClass: CharacterClass[];
    onClassUpdated: (characterClass: CharacterClass[]) => void;
};

export class CharacterClassComponent extends React.Component<Props> {
    private availableClasses: CharacterClass[];
    private readonly availableClassOptions: Array<{ id: string; value: string }>;

    constructor(props: Props) {
        super(props);

        this.availableClasses = [
            {hitDie: "d12", id: "brb", levels: 1, name: "Barbarian"},
            {hitDie: "d8", id: "brd", levels: 1, name: "Bard"},
            {hitDie: "d8", id: "clr", levels: 1, name: "Cleric"},
            {hitDie: "d8", id: "drd", levels: 1, name: "Druid"},
            {hitDie: "d10", id: "ftr", levels: 1, name: "Fighter"},
            {hitDie: "d8", id: "mnk", levels: 1, name: "Monk"},
            {hitDie: "d10", id: "pld", levels: 1, name: "Paladin"},
            {hitDie: "d10", id: "rng", levels: 1, name: "Ranger"},
            {hitDie: "d8", id: "rog", levels: 1, name: "Rogue"},
            {hitDie: "d6", id: "src", levels: 1, name: "Sorcerer"},
            {hitDie: "d8", id: "wrl", levels: 1, name: "Warlock"},
            {hitDie: "d6", id: "wzr", levels: 1, name: "Wizard"}
        ];

        this.availableClassOptions = this.availableClasses.map(this.mapClassesToOptions);
    }

    public updateCharacterClass = (updatedClassId: string): void => {
        const selectedCharacterClass = this.availableClasses
            .filter((availableClass: CharacterClass) => {
                return this.filterCharacterClass(availableClass.id, updatedClassId);
            })[0];
        const updatedCharacterClass = [...this.props.currentClass, selectedCharacterClass];

        this.props.onClassUpdated(updatedCharacterClass);
    }

    public compileClassDisplayValue = (): string => {
        return this.props
            .currentClass
            .map(this.mapClassToDisplayValue)
            .join(", ");
    }

    public compileMultiClassDisplayValue = (): string => {
        const totalClassLevels = this.props
            .currentClass
            .reduce(this.reduceClassLevels, 0);

        return `[Multiple Classes] ${totalClassLevels}`;
    }

    public render(): JSX.Element {
        switch (this.props.currentClass.length) {
            case 0:
                return <CharacterOptionFieldComponent currentValue={this.availableClassOptions[0].value}
                                                      fullWidth={true}
                                                      label="Class"
                                                      onChangeHandler={this.updateCharacterClass}
                                                      options={this.availableClassOptions}
                                                      readOnly={false} />;
            case 1:
            case 2:
                return <CharacterTextFieldComponent currentValue={this.compileClassDisplayValue()}
                                                    fullWidth={true}
                                                    label="Class"
                                                    readOnly={true} />;

            default:
                return <CharacterTextFieldComponent currentValue={this.compileMultiClassDisplayValue()}
                                                    fullWidth={true}
                                                    label="Class"
                                                    readOnly={true} />;
        }
    }

    private mapClassesToOptions = (playableClass: CharacterClass): { id: string; value: string; } => {
        return {
            id: playableClass.id,
            value: playableClass.name
        };
    }

    private filterCharacterClass = (availableClassId: string, selectedClassId: string): boolean => {
        return availableClassId === selectedClassId;
    }

    private mapClassToDisplayValue = (characterClass: CharacterClass): string => {
        return `${characterClass.name} ${characterClass.levels}`;
    }

    private reduceClassLevels = (classLevels: number, currentClass: CharacterClass): number => {
        return classLevels + currentClass.levels;
    }
}
