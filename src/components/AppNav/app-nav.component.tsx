import {Drawer, Hidden, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {AppNavMenuComponent} from "../AppNavMenu/app-nav-menu.component";
import {appNavStyles} from "./app-nav.styles";

interface IProps extends WithStyles<typeof appNavStyles> {
    navIsOpen: boolean;
    onNavMenuOpened: () => void;
}

class AppNavComponent extends React.Component<IProps> {
    public render() {
        return (
            <aside className={this.props.classes.appNav}>
                <Hidden implementation="css"
                        smUp>
                    <Drawer onClose={this.props.onNavMenuOpened}
                            open={this.props.navIsOpen}
                            variant="temporary">
                        <AppNavMenuComponent />
                    </Drawer>
                </Hidden>

                <Hidden implementation="css"
                        xsDown>
                    <Drawer classes={{paper: this.props.classes.appNavDrawerPaper}}
                            open
                            variant="permanent">
                        <AppNavMenuComponent />
                    </Drawer>
                </Hidden>
            </aside>
        );
    }
}

const appNavComponent = withStyles(appNavStyles)(AppNavComponent);

export {appNavComponent as AppNavComponent};
