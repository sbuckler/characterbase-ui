import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const characterAbilityScoresStyles = (): StyleRules => createStyles({
    characterAbilityScores: {
        padding: 8
    },
    characterAbilityScoresField: {
        padding: 10
    }
});
