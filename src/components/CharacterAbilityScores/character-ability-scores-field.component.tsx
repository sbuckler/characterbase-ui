import {FormControl, TextField} from "@material-ui/core";
import * as React from "react";
import {NumberStepperComponent} from "../NumberStepper/number.stepper.component";

type Props = {
    abilityScore?: number;
    label: string;
    fullWidth: boolean;
    onChangeHandler: (abilityScore: number) => void;
    showStepper: boolean;
};

export class CharacterAbilityScoresFieldComponent extends React.Component<Props> {
    public handleOnStep = (abilityScore: number): void => {
        this.props.onChangeHandler(abilityScore);
    }

    public render(): JSX.Element {
        const abilityScore = this.props.abilityScore || 10;
        const abilityScoreModifier = Math.floor((abilityScore - 10) / 2);
        const abilityScoreModifierPrefix = abilityScoreModifier >= 0 ? "+" : "";
        const inputValue = `${abilityScore} (${abilityScoreModifierPrefix}${abilityScoreModifier})`;

        return (
            <FormControl fullWidth={this.props.fullWidth}>
                <TextField InputProps={{disableUnderline: true, readOnly: true}}
                           label={this.props.label}
                           value={inputValue} />

                {
                    this.props.showStepper &&
                    <NumberStepperComponent value={abilityScore}
                                            step={1}
                                            max={20}
                                            min={0}
                                            onStepHandler={this.handleOnStep} />
                }
            </FormControl>
        );
    }
}
