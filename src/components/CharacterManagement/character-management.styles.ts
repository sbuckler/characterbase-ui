import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const characterManagementStyles = (): StyleRules => createStyles({
    addNewCharacterButton: {
        bottom: 30,
        position: "fixed",
        right: 30
    }
});
