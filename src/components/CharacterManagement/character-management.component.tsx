import {Button, withStyles, WithStyles} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import * as React from "react";
import {Character} from "../../types/character";
import {CharacterSheetComponent} from "../CharacterSheet/character-sheet.component";
import {characterManagementStyles} from "./character-management.styles";

interface IProps extends WithStyles<typeof characterManagementStyles> {
    characters: Character[];
    onCharacterAdded: (character: Character) => void;
    onCharacterUpdated: (character: Character) => void;
    onCharacterRemoved: (character: Character) => void;
}

type State = {
    characterToDisplay: Character;
    characters: Array<[Character, boolean]>;
};

class CharacterManagementComponent extends React.Component<IProps, State> {
    public state: State;

    constructor(props: IProps) {
        super(props);
        const mapCharactersToState = (character: Character): [Character, boolean] => {
            return [character, true];
        };

        this.state = {
            characterToDisplay: this.props.characters[0],
            characters: this.props.characters.map(mapCharactersToState)
        };
    }

    public handleCharacterSelected = (selectedCharacter: Character): void => {
        this.setState(() => ({
            characterToDisplay: selectedCharacter
        }));
    }

    public handleUpdatedCharacter = (updatedCharacter: Character): void => {
        this.props.onCharacterUpdated(updatedCharacter);

        this.setState((prevState: State) => {
            const editedCharacters = [...prevState.characters]
                .filter(([character]) => updatedCharacter.id === character.id)
                .map(([character, isNew]) => {
                    return [{...character, ...updatedCharacter}, isNew] as [Character, boolean];
                });

            const restOfCharacters = [...prevState.characters]
                .filter(([character]) => updatedCharacter.id !== character.id);

            const sortedCharacters = [...editedCharacters, ...restOfCharacters]
                .sort((currCharacter, nextCharacter) => {
                    const prevStateIds = prevState.characters
                        .map((character) => character[0].id);

                    const indexOfCurrCharacter = prevStateIds.indexOf(currCharacter[0].id);
                    const indexOfNextCharacter = prevStateIds.indexOf(nextCharacter[0].id);

                    return indexOfCurrCharacter - indexOfNextCharacter || prevState.characters.length;
                });

            return {
                characterToDisplay: editedCharacters[0][0],
                characters: sortedCharacters
            };
        });
    }

    public handleAddCharacter = (): void => {
        const newCharacter = this.createNewCharacter();

        this.props.onCharacterAdded(newCharacter);

        this.setState((prevState: State) => ({
            characterToDisplay: newCharacter,
            characters: [...prevState.characters, [newCharacter, true]]
        } as State));
    }

    public render(): JSX.Element {
        return (
            <>
                <CharacterSheetComponent character={this.state.characterToDisplay}
                                         characterOptions={this.state.characters}
                                         key={this.state.characterToDisplay.id}
                                         newCharacter={true}
                                         onCharacterSelected={this.handleCharacterSelected}
                                         onCharacterUpdated={this.handleUpdatedCharacter} />

                <Button className={this.props.classes.addNewCharacterButton}
                        color="primary"
                        onClick={this.handleAddCharacter}
                        variant="fab">
                    <AddIcon />
                </Button>
            </>
        );
    }

    private createNewCharacter = (): Character => {
        return {
            abilityScores: {
                charisma: 10,
                constitution: 10,
                dexterity: 10,
                intelligence: 10,
                strength: 10,
                wisdom: 10
            },
            armorClass: 10,
            attacks: [{
                attackBonus: "+2",
                damage: "1d4 + 0",
                damageType: {
                    id: "bld",
                    name: "Bludgeoning"
                },
                id: Date.now().toString(),
                name: "Unarmed Strike",
                properties: ""
            }],
            currency: {
                copper: 0,
                electrum: 0,
                gold: 0,
                platinum: 0,
                silver: 0
            },
            deathSaves: {
                failures: 0,
                successes: 0
            },
            details: {
                alignment: {
                    id: "nn",
                    name: "Neutral"
                },
                background: "",
                class: [],
                experiencePoints: 0,
                name: "",
                playerName: "",
                race: {
                    id: "hmn",
                    name: "Human"
                }
            },
            hitDice: [],
            hitPoints: {
                current: 1,
                maximum: 1,
                temporary: 0
            },
            id: Date.now().toString(),
            initiative: 0,
            inspiration: false,
            proficiencyBonus: 2,
            savingThrows: {
                charisma: {
                    bonuses: [],
                    proficient: false
                },
                constitution: {
                    bonuses: [],
                    proficient: false
                },
                dexterity: {
                    bonuses: [],
                    proficient: false
                },
                intelligence: {
                    bonuses: [],
                    proficient: false
                },
                strength: {
                    bonuses: [],
                    proficient: false
                },
                wisdom: {
                    bonuses: [],
                    proficient: false
                }
            },
            skills: {
                acrobatics: {
                    bonuses: [],
                    proficient: false
                },
                animalHandling: {
                    bonuses: [],
                    proficient: false
                },
                arcana: {
                    bonuses: [],
                    proficient: false
                },
                athletics: {
                    bonuses: [],
                    proficient: false
                },
                deception: {
                    bonuses: [],
                    proficient: false
                },
                history: {
                    bonuses: [],
                    proficient: false
                },
                insight: {
                    bonuses: [],
                    proficient: false
                },
                intimidation: {
                    bonuses: [],
                    proficient: false
                },
                investigation: {
                    bonuses: [],
                    proficient: false
                },
                medicine: {
                    bonuses: [],
                    proficient: false
                },
                nature: {
                    bonuses: [],
                    proficient: false
                },
                perception: {
                    bonuses: [],
                    proficient: false
                },
                performance: {
                    bonuses: [],
                    proficient: false
                },
                persuasion: {
                    bonuses: [],
                    proficient: false
                },
                religion: {
                    bonuses: [],
                    proficient: false
                },
                sleightOfHand: {
                    bonuses: [],
                    proficient: false
                },
                stealth: {
                    bonuses: [],
                    proficient: false
                },
                survival: {
                    bonuses: [],
                    proficient: false
                }
            },
            speed: 30,
            traits: {
                bonds: "",
                flaws: "",
                ideals: "",
                personalityTraits: ""
            }
        };
    }
}

const characterManagementComponent = withStyles(characterManagementStyles)(CharacterManagementComponent);

export {characterManagementComponent as CharacterManagementComponent};
