import {
    AppBar,
    Grid,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Toolbar,
    Typography,
    withStyles,
    WithStyles
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import LocalMoviesIcon from "@material-ui/icons/LocalMovies";
import PeopleIcon from "@material-ui/icons/People";
import * as React from "react";
import {appNavMenuStyles} from "./app-nav-menu.styles";

interface IProps extends WithStyles<typeof appNavMenuStyles> {
}

const AppNavMenuComponent: React.SFC<IProps> = (props: IProps): JSX.Element => {
    return (
        <nav className={props.classes.appNavMenu}>
            <AppBar className={props.classes.appNavMenuAppBar}
                    position="static">
                <Toolbar variant="dense">
                    <Grid container>
                        <Grid item
                              xs={3} />

                        <Grid item
                              xs={9}>
                            <Typography style={{color: "#ffffff"}}
                                        variant="h6">
                                Group Name <ExpandMoreIcon style={{verticalAlign: "middle"}} />
                            </Typography>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>

            <Grid className={props.classes.appNavGridContainer}
                  container>
                <Grid item
                      xs={3}>
                    <List className={props.classes.groupsMenu}>
                        <ListItem button>
                            (Dope)
                        </ListItem>

                        <ListItem button>
                            (Word)
                        </ListItem>
                    </List>
                </Grid>

                <Grid item
                      xs={9}>
                    <List className={props.classes.channelsMenu}>
                        <ListItem button>
                            <ListItemIcon>
                                <LocalMoviesIcon />
                            </ListItemIcon>

                            <ListItemText primary="Campaigns" />
                        </ListItem>

                        <ListItem button>
                            <ListItemIcon>
                                <PeopleIcon />
                            </ListItemIcon>

                            <ListItemText primary="Characters" />
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
        </nav>
    );
};

const appNavMenuComponent = withStyles(appNavMenuStyles)(AppNavMenuComponent);

export {appNavMenuComponent as AppNavMenuComponent};
