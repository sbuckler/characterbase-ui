import {Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {characterArmorClassStyles} from "./character-armor-class.styles";

type Props = WithStyles<typeof characterArmorClassStyles> & {
    armorClass: number;
};

const CharacterArmorClassComponent: React.SFC<Props> = (props: Props): JSX.Element => {
    return (
        <Paper className={props.classes.characterArmorClass}>
            <Typography color="primary"
                        component="legend"
                        variant="overline">
                Armor Class
            </Typography>

            <Grid container>
                <Grid className={props.classes.characterArmorClassField}
                      item
                      xs={12}>
                    <Typography align="center"
                                variant="h5">
                        {props.armorClass}
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};

const characterClassComponent = withStyles(characterArmorClassStyles)(CharacterArmorClassComponent);

export {characterClassComponent as CharacterArmorClassComponent};
