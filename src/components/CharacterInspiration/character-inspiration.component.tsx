import {Checkbox, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import * as React from "react";
import {characterInspirationStyles} from "./character-inspiration.styles";

type Props = WithStyles<typeof characterInspirationStyles> & {
    inspired: boolean;
    onInspirationUpdated: (inspiration: boolean) => void;
};

class CharacterInspirationComponent extends React.Component<Props> {
    public handleOnChange = (event: React.FormEvent): void => {
        const eventTarget = event.target as HTMLInputElement;

        this.props.onInspirationUpdated(eventTarget.checked);
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterInspiration}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Inspiration
                </Typography>

                <Grid alignContent="center"
                      container>
                    <Grid className={this.props.classes.characterInspirationField}
                          item
                          xs={12}>
                        <Checkbox checked={this.props.inspired}
                                  checkedIcon={<CheckBoxIcon color="primary"
                                                             fontSize="large" />}
                                  className={this.props.classes.characterInspirationCheckbox}
                                  icon={<CheckBoxOutlineBlankIcon fontSize="large" />}
                                  onChange={this.handleOnChange} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

const characterInspirationComponent = withStyles(characterInspirationStyles)(CharacterInspirationComponent);

export {characterInspirationComponent as CharacterInspirationComponent};
