import {createStyles} from "@material-ui/core";

export const characterInitiativeStyles = createStyles({
    characterInitiative: {
        padding: 8
    },
    characterInitiativeField: {
        padding: 10
    }
});
