import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";
import {CharacterBaseTheme} from "../App/app.theme";

export const characterSkillsStyles = (theme: CharacterBaseTheme): StyleRules => createStyles({
    characterSkills: {
        padding: 8
    },
    characterSkillsCheckbox: {
        padding: 0
    },
    characterSkillsField: {
        padding: "8px 0"
    },
    characterSkillsTextField: {
        paddingLeft: theme.spacing.unit
    }
});
