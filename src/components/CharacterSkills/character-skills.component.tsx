import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterSkills} from "../../types/character-skills";
import {CharacterSkillsFieldComponent} from "./character-skills-field.component";
import {characterSkillsStyles} from "./character-skilsl.styles";

interface IProps extends WithStyles<typeof characterSkillsStyles> {
    newCharacter: boolean;
    proficiencyBonus: number;
    skills: CharacterSkills;
}

class CharacterSkillsComponent extends React.Component<IProps> {
    public compileCharacterSkillFields = (): JSX.Element[] => {
        return Object.keys(this.props.skills)
            .map(this.mapSkillsToJsx)
            .map(this.mapComponentToGridItem);
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterSkills}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Skills
                </Typography>

                <Grid container
                      direction="column">
                    {this.compileCharacterSkillFields()}
                </Grid>
            </Paper>
        );
    }

    private mapSkillsToJsx = (skillName: keyof CharacterSkills): JSX.Element => {
        const skill = this.props.skills[skillName];
        const labelName = this.convertCamelCaseFieldToLabel(skillName);
        const proficiencyBonus = skill.proficient ? this.props.proficiencyBonus : 0;

        return (
            <CharacterSkillsFieldComponent label={labelName}
                                           proficiencyBonus={proficiencyBonus}
                                           skill={skill} />
        );
    }

    private mapComponentToGridItem = (component: JSX.Element, index: number): JSX.Element => {
        return (
            <React.Fragment key={index}>
                {index > 0 && <Divider />}
                <Grid className={this.props.classes.characterSkillsField}
                      item>
                    {component}
                </Grid>
            </React.Fragment>
        );
    }

    private convertCamelCaseFieldToLabel = (fieldName: string): string => {
        return fieldName.replace(/([A-Z])/g, " $1")
            .replace(/^./, (firstCharacter: string) => firstCharacter.toUpperCase());
    }
}

const characterSkillsComponent = withStyles(characterSkillsStyles)(CharacterSkillsComponent);

export {characterSkillsComponent as CharacterSkillsComponent};
