import {Grid, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {Character} from "../../types/character";
import {CharacterAbilityScores} from "../../types/character-ability-scores";
import {CharacterAttack} from "../../types/character-attack";
import {CharacterCurrency} from "../../types/character-currency";
import {CharacterDeathSaves} from "../../types/character-death-saves";
import {CharacterDetails} from "../../types/character-details";
import {CharacterHitDice} from "../../types/character-hit-dice";
import {CharacterHitPoints} from "../../types/character-hit-points";
import {CharacterSavingThrows} from "../../types/character-saving-throws";
import {CharacterTraits} from "../../types/character-traits";
import {CharacterAbilityScoresComponent} from "../CharacterAbilityScores/character-ability-scores.component";
import {CharacterArmorClassComponent} from "../CharacterArmorClass/character-armor-class.component";
import {CharacterAttacksComponent} from "../CharacterAttacks/character-attacks.component";
import {CharacterCurrencyComponent} from "../CharacterCurrency/character-currency.component";
import {CharacterDeathSavesComponent} from "../CharacterDeathSaves/character-death-saves.component";
import {CharacterDetailsComponent} from "../CharacterDetails/character-details.component.";
import {CharacterHitDiceComponent} from "../CharacterHitDice/character-hit-dice.component";
import {CharacterHitPointsComponent} from "../CharacterHitPoints/character-hit-points.component";
import {CharacterInitiativeComponent} from "../CharacterInitiative/character-initiative.component";
import {CharacterInspirationComponent} from "../CharacterInspiration/character-inspiration.component";
import {CharacterProficiencyComponent} from "../CharacterProficiency/character-proficiency.component";
import {CharacterSavingThrowsComponent} from "../CharacterSavingThrows/character-saving-throws.component";
import {CharacterSelectionComponent} from "../CharacterSelection/character-selection.component";
import {CharacterSkillsComponent} from "../CharacterSkills/character-skills.component";
import {CharacterSpeedComponent} from "../CharacterSpeed/character-speed.component";
import {CharacterTraitsComponent} from "../CharacterTraits/character-traits.component";
import {characterSheetStyles} from "./character-sheet.styles";

interface IProps extends WithStyles<typeof characterSheetStyles> {
    character: Character;
    characterOptions: Array<[Character, boolean]>;
    newCharacter: boolean;
    onCharacterSelected: (character: Character) => void;
    onCharacterUpdated: (character: Character) => void;
}

class CharacterSheetComponent extends React.Component<IProps> {
    private readonly characterSelectOptions: Character[];

    constructor(props: IProps) {
        super(props);

        this.characterSelectOptions = props.characterOptions
            .map(this.mapCharacterOptionsToCharacters);
    }

    public handleOnSubmit = (event: React.FormEvent): void => {
        event.preventDefault();
    }

    public handleCharacterSelected = (selectedCharacter: Character): void => {
        this.props.onCharacterSelected(selectedCharacter);
    }

    public handleInspirationUpdated = (updatedInspiration: boolean): void => {
        this.props.onCharacterUpdated({...this.props.character, inspiration: updatedInspiration});
    }

    public handleAbilityScoresUpdated = (updatedAbilityScores: CharacterAbilityScores): void => {
        this.props.onCharacterUpdated({...this.props.character, abilityScores: updatedAbilityScores});
    }

    public handleDetailsUpdated = (updatedDetails: CharacterDetails): void => {
        this.props.onCharacterUpdated({...this.props.character, details: updatedDetails});
    }

    public handleSavingThrowsUpdated = (updatedSavingThrows: CharacterSavingThrows): void => {
        this.props.onCharacterUpdated({...this.props.character, savingThrows: updatedSavingThrows});
    }

    public handleHitPointsUpdated = (updatedHitPoints: CharacterHitPoints): void => {
        this.props.onCharacterUpdated({...this.props.character, hitPoints: updatedHitPoints});
    }

    public handleHitDiceUpdated = (updatedHitDice: CharacterHitDice): void => {
        this.props.onCharacterUpdated({...this.props.character, hitDice: updatedHitDice});
    }

    public handleDeathSavesUpdated = (updatedDeathSaves: CharacterDeathSaves): void => {
        this.props.onCharacterUpdated({...this.props.character, deathSaves: updatedDeathSaves});
    }

    public handleAttacksUpdated = (updatedAttacks: CharacterAttack[]): void => {
        this.props.onCharacterUpdated({...this.props.character, attacks: updatedAttacks});
    }

    public handleTraitsUpdated = (updatedTraits: CharacterTraits): void => {
        this.props.onCharacterUpdated({...this.props.character, traits: updatedTraits});
    }

    public handleCurrencyUpdated = (updatedCurrency: CharacterCurrency): void => {
        this.props.onCharacterUpdated({...this.props.character, currency: updatedCurrency});
    }

    public render(): JSX.Element {
        return (
            <Grid className={this.props.classes.characterSheet}
                  component="form"
                  container
                  justify="center"
                  onSubmit={this.handleOnSubmit}>
                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={2}
                      xs={12}>
                    <CharacterSelectionComponent characters={this.characterSelectOptions}
                                                 currentCharacter={this.props.character}
                                                 onCharacterSelected={this.handleCharacterSelected} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={10}
                      xs={12}>
                    <CharacterDetailsComponent details={this.props.character.details}
                                               onDetailsUpdated={this.handleDetailsUpdated}
                                               newCharacter={this.props.newCharacter} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={12}>
                    <CharacterAbilityScoresComponent abilityScores={this.props.character.abilityScores}
                                                     newCharacter={this.props.newCharacter}
                                                     onAbilityScoresUpdated={this.handleAbilityScoresUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterInspirationComponent inspired={this.props.character.inspiration}
                                                   onInspirationUpdated={this.handleInspirationUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterProficiencyComponent proficiencyBonus={this.props.character.proficiencyBonus} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterSavingThrowsComponent proficiencyBonus={this.props.character.proficiencyBonus}
                                                    onSavingThrowsUpdated={this.handleSavingThrowsUpdated}
                                                    savingThrows={this.props.character.savingThrows} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterSkillsComponent newCharacter={this.props.newCharacter}
                                              proficiencyBonus={this.props.character.proficiencyBonus}
                                              skills={this.props.character.skills} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterArmorClassComponent armorClass={this.props.character.armorClass} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterInitiativeComponent initiative={this.props.character.initiative} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterSpeedComponent speed={this.props.character.speed} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterHitPointsComponent hitPoints={this.props.character.hitPoints}
                                                 onHitPointsUpdated={this.handleHitPointsUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={2}
                      xs={6}>
                    <CharacterHitDiceComponent hitDice={this.props.character.hitDice}
                                               onHitDiceUpdated={this.handleHitDiceUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={2}
                      xs={6}>
                    <CharacterDeathSavesComponent deathSaves={this.props.character.deathSaves}
                                                  onDeathSavesUpdated={this.handleDeathSavesUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={4}
                      xs={12}>
                    <CharacterAttacksComponent attacks={this.props.character.attacks}
                                               onAttacksUpdated={this.handleAttacksUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={4}
                      xs={12}>
                    <CharacterTraitsComponent traits={this.props.character.traits}
                                              newCharacter={this.props.newCharacter}
                                              onTraitsUpdated={this.handleTraitsUpdated} />
                </Grid>

                <Grid className={this.props.classes.characterSheetGridItem}
                      component="fieldset"
                      item
                      sm={1}
                      xs={6}>
                    <CharacterCurrencyComponent currency={this.props.character.currency}
                                                onCurrencyUpdated={this.handleCurrencyUpdated} />
                </Grid>
            </Grid>
        );
    }

    private mapCharacterOptionsToCharacters = ([character]: [Character, boolean]): Character => {
        return character;
    }
}

const characterSheetComponent = withStyles(characterSheetStyles)(CharacterSheetComponent);

export {characterSheetComponent as CharacterSheetComponent};
