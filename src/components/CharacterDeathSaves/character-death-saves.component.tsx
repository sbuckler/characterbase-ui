import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterDeathSaves} from "../../types/character-death-saves";
import {CharacterDeathSavesFieldComponent} from "./character-death-saves-field.component";
import {characterDeathSavesStyles} from "./character-death-saves.styles";

type Props = WithStyles<typeof characterDeathSavesStyles> & {
    deathSaves: CharacterDeathSaves;
    onDeathSavesUpdated: (deathSaves: CharacterDeathSaves) => void;
};

class CharacterDeathSavesComponent extends React.Component<Props> {
    public handleSuccessDeathSave = (successCount: number): void => {
        this.props.onDeathSavesUpdated({...this.props.deathSaves, successes: successCount});
    }

    public handleFailedDeathSave = (failureCount: number): void => {
        this.props.onDeathSavesUpdated({...this.props.deathSaves, failures: failureCount});
    }

    public render() {
        return (
            <Paper className={this.props.classes.characterDeathSaves}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Death Saves
                </Typography>

                <Grid container
                      direction="column">
                    <Grid className={this.props.classes.characterDeathSavesField}
                          item
                          xs={12}>
                        <CharacterDeathSavesFieldComponent label="Successes"
                                                           onDeathSaveUpdated={this.handleSuccessDeathSave}
                                                           saveCount={this.props.deathSaves.successes} />
                    </Grid>

                    <Divider />

                    <Grid className={this.props.classes.characterDeathSavesField}
                          item
                          xs={12}>
                        <CharacterDeathSavesFieldComponent label="Failures"
                                                           onDeathSaveUpdated={this.handleFailedDeathSave}
                                                           saveCount={this.props.deathSaves.failures} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

const characterDeathSavesComponent = withStyles(characterDeathSavesStyles)(CharacterDeathSavesComponent);

export {characterDeathSavesComponent as CharacterDeathSavesComponent};
