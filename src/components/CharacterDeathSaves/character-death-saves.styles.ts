import {createStyles} from "@material-ui/core";

export const characterDeathSavesStyles = createStyles({
    characterDeathSaves: {
        padding: 8
    },
    characterDeathSavesField: {
        padding: 10
    }
});
