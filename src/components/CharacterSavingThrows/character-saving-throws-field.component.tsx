import {Checkbox, FormControl, Grid, TextField, withStyles, WithStyles} from "@material-ui/core";
import {RadioButtonChecked, RadioButtonUnchecked} from "@material-ui/icons";
import * as React from "react";
import {CharacterSavingThrow} from "../../types/character-saving-throws";
import {CharacterStatBonus} from "../../types/character-stat-bonus";
import {characterSavingThrowsStyles} from "./character-saving-throws.styles";

type Props = WithStyles<typeof characterSavingThrowsStyles> & {
    label: string;
    proficiencyBonus: number;
    savingThrow: CharacterSavingThrow;
};

class CharacterSavingThrowsFieldComponent extends React.Component<Props> {
    public readonly modifier: number;

    constructor(props: Props) {
        super(props);

        const reduceBonusSeed = props.savingThrow.proficient ? props.proficiencyBonus : 0;

        this.modifier = props.savingThrow
            .bonuses
            .reduce(this.reduceBonus, reduceBonusSeed);
    }

    public render(): JSX.Element {
        const modifierPrefix = this.modifier >= 0 ? "+" : "";

        return (
            <FormControl>
                <Grid container
                      alignItems="baseline">
                    <Grid item
                          xs={2}>
                        <Checkbox checked={this.props.proficiencyBonus > 0}
                                  checkedIcon={<RadioButtonChecked />}
                                  className={this.props.classes.characterSavingThrowsCheckbox}
                                  color="primary"
                                  icon={<RadioButtonUnchecked />}
                                  readOnly={true} />
                    </Grid>

                    <Grid item
                          xs={10}>
                        <TextField className={this.props.classes.characterSavingThrowsTextField}
                                   InputProps={{disableUnderline: true, readOnly: true}}
                                   fullWidth={true}
                                   label={this.props.label}
                                   value={`${modifierPrefix}${this.modifier}`} />
                    </Grid>
                </Grid>
            </FormControl>
        );
    }

    private reduceBonus = (previousModifier: number, currentBonus: CharacterStatBonus): number => {
        return previousModifier + currentBonus.modifier;
    }
}

const withCharacterSavingThrowsStyles = withStyles(characterSavingThrowsStyles);
const characterSavingThrowsFieldComponent = withCharacterSavingThrowsStyles(CharacterSavingThrowsFieldComponent);

export {characterSavingThrowsFieldComponent as CharacterSavingThrowsFieldComponent};
