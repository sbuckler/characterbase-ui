import {Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterAbilityScores} from "../../types/character-ability-scores";
import {CharacterSavingThrows} from "../../types/character-saving-throws";
import {CharacterSavingThrowsFieldComponent} from "./character-saving-throws-field.component";
import {characterSavingThrowsStyles} from "./character-saving-throws.styles";

interface IProps extends WithStyles<typeof characterSavingThrowsStyles> {
    proficiencyBonus: number;
    savingThrows: CharacterSavingThrows;
    onSavingThrowsUpdated: (savingThrows: CharacterSavingThrows) => void;
}

type SortedSavingThrow = {
    name: keyof CharacterAbilityScores;
    sortOrder: number;
};

class CharacterSavingThrowsComponent extends React.Component<IProps> {
    public compileSavingThrowFields = (): JSX.Element[] => {
        return Object.keys(this.props.savingThrows)
            .map(this.mapSavingThrowsToSortOrder)
            .sort(this.sortSavingThrows)
            .map(this.mapSavingThrowsToJsx)
            .map(this.mapComponentToGridItem);
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterSavingThrows}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Saving Throws
                </Typography>

                <Grid container
                      direction="column">
                    {this.compileSavingThrowFields()}
                </Grid>
            </Paper>
        );
    }

    private mapSavingThrowsToSortOrder = (savingThrowName: keyof CharacterAbilityScores): SortedSavingThrow => {
        switch (savingThrowName) {
            case "strength":
                return {name: savingThrowName, sortOrder: 1};
            case "dexterity":
                return {name: savingThrowName, sortOrder: 2};
            case "constitution":
                return {name: savingThrowName, sortOrder: 3};
            case "intelligence":
                return {name: savingThrowName, sortOrder: 4};
            case "wisdom":
                return {name: savingThrowName, sortOrder: 5};
            case "charisma":
                return {name: savingThrowName, sortOrder: 6};
            default:
                return {name: savingThrowName, sortOrder: 0};
        }
    }

    private sortSavingThrows = (currSavingThrow: SortedSavingThrow, nextSavingThrow: SortedSavingThrow): number => {
        return currSavingThrow.sortOrder - nextSavingThrow.sortOrder;
    }

    private mapSavingThrowsToJsx = (sortedSavingThrow: SortedSavingThrow): JSX.Element => {
        const savingThrow = this.props.savingThrows[sortedSavingThrow.name];
        const labelName = this.convertCamelCaseFieldToLabel(sortedSavingThrow.name);
        const proficiencyBonus = this.props.savingThrows[sortedSavingThrow.name].proficient
            ? this.props.proficiencyBonus
            : 0;

        return (
            <CharacterSavingThrowsFieldComponent label={labelName}
                                                 proficiencyBonus={proficiencyBonus}
                                                 savingThrow={savingThrow} />
        );
    }

    private mapComponentToGridItem = (component: JSX.Element, index: number): JSX.Element => {
        return (
            <React.Fragment key={index}>
                {index > 0 && <Divider />}
                <Grid className={this.props.classes.characterSavingThrowsField}
                      item>
                    {component}
                </Grid>
            </React.Fragment>
        );
    }

    private convertCamelCaseFieldToLabel = (fieldName: string): string => {
        return fieldName.replace(/([A-Z])/g, " $1")
            .replace(/^./, (firstCharacter: string) => firstCharacter.toUpperCase());
    }
}

const characterSavingThrowsComponent = withStyles(characterSavingThrowsStyles)(CharacterSavingThrowsComponent);

export {characterSavingThrowsComponent as CharacterSavingThrowsComponent};
