import {createStyles} from "@material-ui/core";

export const characterProficiencyStyles = createStyles({
    characterProficiency: {
        padding: 8
    },
    characterProficiencyField: {
        padding: 10
    }
});
