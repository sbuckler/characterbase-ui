import {Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {characterProficiencyStyles} from "./character-proficiency.styles";

type Props = WithStyles<typeof characterProficiencyStyles> & {
    proficiencyBonus: number;
};

const CharacterProficiencyComponent: React.SFC<Props> = (props: Props): JSX.Element => {
    return (
        <Paper className={props.classes.characterProficiency}>
            <Typography color="primary"
                        component="legend"
                        variant="overline">
                Proficiency Bonus
            </Typography>

            <Grid container>
                <Grid className={props.classes.characterProficiencyField}
                      item
                      xs={12}>
                    <Typography align="center"
                                variant="h5">
                        +{props.proficiencyBonus}
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};

const characterProficiencyComponent = withStyles(characterProficiencyStyles)(CharacterProficiencyComponent);

export {characterProficiencyComponent as CharacterProficiencyComponent};
