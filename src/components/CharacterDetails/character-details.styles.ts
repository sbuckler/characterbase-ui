import {createStyles} from "@material-ui/core";

export const characterDetailsStyles = createStyles({
    characterDetails: {
        padding: 8
    },
    characterDetailsField: {
        padding: 10
    }
});
