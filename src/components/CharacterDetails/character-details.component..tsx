import {Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterAlignment} from "../../types/character-alignment";
import {CharacterClass} from "../../types/character-class";
import {CharacterDetails} from "../../types/character-details";
import {CharacterRace} from "../../types/character-race";
import {CharacterClassComponent} from "../CharacterClass/character-class.component";
import {CharacterOptionFieldComponent} from "../CharacterOptionField/character-option.field.component";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";
import {characterDetailsStyles} from "./character-details.styles";

type Props = WithStyles<typeof characterDetailsStyles> & {
    details: CharacterDetails;
    newCharacter: boolean;
    onDetailsUpdated: (updatedCharacterDetails: CharacterDetails) => void;
};

class CharacterDetailsComponent extends React.Component<Props> {
    private readonly availableAlignments: CharacterAlignment[];
    private readonly availableRaces: CharacterRace[];
    private readonly alignments: Array<{ id: string; value: string }>;
    private readonly racesOptions: Array<{ id: string; value: string }>;

    constructor(props: Props) {
        super(props);

        this.availableAlignments = [
            {id: "lg", name: "Lawful Good"},
            {id: "ln", name: "Lawful Neutral"},
            {id: "le", name: "Lawful Evil"},
            {id: "ng", name: "Neutral Good"},
            {id: "nn", name: "Neutral"},
            {id: "ne", name: "Neutral Evil"},
            {id: "cg", name: "Chaotic Good"},
            {id: "cn", name: "Chaotic Neutral"},
            {id: "ce", name: "Chaotic Evil"}
        ];

        this.alignments = this.availableAlignments
            .map(this.mapAlignmentsToOptions);

        this.availableRaces = [
            {id: "hmn", name: "Human"},
            {id: "drg", name: "Dragonborn"},
            {id: "elf", name: "Elf"},
            {id: "hfo", name: "Half-orc"},
            {id: "hfe", name: "Half-elf"},
            {id: "hfl", name: "Halfling"},
            {id: "gnm", name: "Gnome"},
            {id: "tfl", name: "Tiefling"}
        ];

        this.racesOptions = this.availableRaces
            .map(this.mapRacesToOptions);
    }

    public updatePlayerName = (updatedPlayerName: string): void => {
        this.props.onDetailsUpdated({...this.props.details, playerName: updatedPlayerName});
    }

    public updateCharacterClass = (updatedClass: CharacterClass[]): void => {
        this.props.onDetailsUpdated({...this.props.details, class: updatedClass});
    }

    public updateCharacterBackground = (characterBackground: string): void => {
        this.props.onDetailsUpdated({...this.props.details, background: characterBackground});
    }

    public updateCharacterRace = (updatedRaceId: string): void => {
        const updatedRace = this.availableRaces
            .filter((race: CharacterRace) => this.filterMatchingRaces(race, updatedRaceId))[0];

        this.props.onDetailsUpdated({...this.props.details, race: updatedRace});
    }

    public updateCharacterAlignment = (updatedAlignmentId: string): void => {
        const updatedAlignment = this.availableAlignments
            .filter((alignment: CharacterAlignment) => {
                return this.filterMatchingAlignments(alignment, updatedAlignmentId);
            })[0];

        this.props.onDetailsUpdated({...this.props.details, alignment: updatedAlignment});
    }

    public updateCharacterExperience = (characterExperience: number): void => {
        this.props.onDetailsUpdated({...this.props.details, experiencePoints: characterExperience});
    }

    public render(): JSX.Element {
        return (
            <Paper className={this.props.classes.characterDetails}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Details
                </Typography>

                <Grid container>
                    <Grid className={this.props.classes.characterDetailsField}
                          item
                          sm={2}
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.details.playerName}
                                                     fullWidth={true}
                                                     label="Player Name"
                                                     onChangeHandler={this.updatePlayerName}
                                                     readOnly={!this.props.newCharacter} />
                    </Grid>

                    <Grid className={this.props.classes.characterDetailsField}
                          item
                          sm={2}
                          xs={12}>
                        <CharacterClassComponent currentClass={this.props.details.class}
                                                 onClassUpdated={this.updateCharacterClass} />
                    </Grid>

                    <Grid className={this.props.classes.characterDetailsField}
                          item
                          sm={2}
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.details.background}
                                                     fullWidth={true}
                                                     label="Background"
                                                     readOnly={!this.props.newCharacter}
                                                     onChangeHandler={this.updateCharacterBackground} />
                    </Grid>

                    <Grid className={this.props.classes.characterDetailsField}
                          item
                          sm={2}
                          xs={12}>
                        <CharacterOptionFieldComponent currentValue={this.props.details.race.id}
                                                       fullWidth={true}
                                                       label="Race"
                                                       onChangeHandler={this.updateCharacterRace}
                                                       options={this.racesOptions}
                                                       readOnly={!this.props.newCharacter} />
                    </Grid>

                    <Grid className={this.props.classes.characterDetailsField}
                          item
                          sm={2}
                          xs={12}>
                        <CharacterOptionFieldComponent currentValue={this.props.details.alignment.id}
                                                       fullWidth={true}
                                                       label="Alignment"
                                                       onChangeHandler={this.updateCharacterAlignment}
                                                       options={this.alignments}
                                                       readOnly={!this.props.newCharacter} />
                    </Grid>

                    <Grid className={this.props.classes.characterDetailsField}
                          item
                          sm={2}
                          xs={12}>
                        <CharacterTextFieldComponent currentValue={this.props.details.experiencePoints}
                                                     fullWidth={true}
                                                     label="Experience Points"
                                                     onChangeHandler={this.updateCharacterExperience}
                                                     readOnly={!this.props.newCharacter} />
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    private mapAlignmentsToOptions = (alignment: { id: string; name: string; }): { id: string; value: string; } => {
        return {
            id: alignment.id,
            value: alignment.name
        };
    }

    private mapRacesToOptions = (race: CharacterRace): { id: string; value: string } => {
        return {
            id: race.id,
            value: race.name
        };
    }

    private filterMatchingRaces = (race: CharacterRace, idToMatch: string): boolean => {
        return race.id === idToMatch;
    }

    private filterMatchingAlignments = (alignment: CharacterAlignment, idToMatch: string): boolean => {
        return alignment.id === idToMatch;
    }
}

const characterDetailsComponent = withStyles(characterDetailsStyles)(CharacterDetailsComponent);

export {characterDetailsComponent as CharacterDetailsComponent};
