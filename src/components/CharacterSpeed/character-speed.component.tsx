import {Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {characterSpeedStyles} from "./character-speed.styles";

type Props = WithStyles<typeof characterSpeedStyles> & {
    speed: number;
};

const CharacterSpeedComponent: React.SFC<Props> = (props: Props): JSX.Element => {
    return (
        <Paper className={props.classes.characterSpeed}>
            <Typography color="primary"
                        component="legend"
                        variant="overline">
                Speed
            </Typography>

            <Grid container>
                <Grid className={props.classes.characterSpeedField}
                      item
                      xs={12}>
                    <Typography align="center"
                                variant="h5">
                        {`${props.speed}ft`}
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};

const characterSpeedComponent = withStyles(characterSpeedStyles)(CharacterSpeedComponent);

export {characterSpeedComponent as CharacterSpeedComponent};
