import {LinearProgress, Toolbar, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharactersProvider} from "../../providers/characters.provider";
import {CharacterManagementComponent} from "../CharacterManagement/character-management.component";
import {appBodyStyles} from "./app-body.styles";

interface IProps extends WithStyles<typeof appBodyStyles> {
}

class AppBodyComponent extends CharactersProvider<IProps> {
    public render(): JSX.Element {
        return (
            <main className={this.props.classes.appBody}>
                {this.state.loading
                    ? <LinearProgress color="secondary" />
                    : <CharacterManagementComponent characters={this.characters}
                                                    onCharacterAdded={this.handleAddCharacter}
                                                    onCharacterUpdated={this.handleCharacterUpdated}
                                                    onCharacterRemoved={this.handleCharacterRemoved} />
                }
            </main>
        );
    }
}

const appBodyComponent = withStyles(appBodyStyles)(AppBodyComponent);

export {appBodyComponent as AppBodyComponent};
