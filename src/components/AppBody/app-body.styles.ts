import {createStyles} from "@material-ui/core";

export const appBodyStyles = createStyles({
    appBody: {
        flexGrow: 1,
        height: `calc(100vh - 48px)`,
        marginTop: 48,
        overflowY: "scroll"
    }
});
