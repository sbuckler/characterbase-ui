import {Button, Divider, Grid, Paper, Typography, withStyles, WithStyles} from "@material-ui/core";
import * as React from "react";
import {CharacterAttack} from "../../types/character-attack";
import {CharacterAttackFieldComponent} from "./character-attack-field.component";
import {characterAttackStyles} from "./character-attacks.styles";

type Props = WithStyles<typeof characterAttackStyles> & {
    attacks: CharacterAttack[];
    onAttacksUpdated: (attacks: CharacterAttack[]) => void;
};

class CharacterAttacksComponent extends React.Component<Props> {
    public compileAttackFields = (): JSX.Element[] => {
        return this.props
            .attacks
            .sort(this.sortAttacksByAttackBonus)
            .map(this.mapAttacksToJsx)
            .map(this.mapComponentToGridItem);
    }

    public handleAttackUpdated = (updatedAttack: CharacterAttack): void => {
        const editedAttack = this.props
            .attacks
            .filter((attack: CharacterAttack): boolean => attack.id === updatedAttack.id)
            .map((attack: CharacterAttack) => ({...attack, ...updatedAttack}));

        const remainingAttacks = this.props
            .attacks
            .filter((attack: CharacterAttack): boolean => attack.id !== updatedAttack.id);

        this.props.onAttacksUpdated([...remainingAttacks, ...editedAttack].sort(this.sortAttacksByAttackBonus));
    }

    public handleAttackAdded = (): void => {
        const addedAttack = {
            attackBonus: "",
            damage: "",
            damageType: {
                id: "bld",
                name: "Bludgeoning"
            },
            id: Date.now().toString(),
            name: "",
            properties: ""
        } as CharacterAttack;

        this.props.onAttacksUpdated([...this.props.attacks, addedAttack]);
    }

    public handleAttackDeleted = (attackId: string): void => {
        const remainingAttacks = this.props
            .attacks
            .filter((attack: CharacterAttack): boolean => attack.id !== attackId);

        this.props.onAttacksUpdated([...remainingAttacks].sort(this.sortAttacksByAttackBonus));
    }

    public render() {
        return (
            <Paper className={this.props.classes.characterAttacks}>
                <Typography color="primary"
                            component="legend"
                            variant="overline">
                    Attacks
                </Typography>

                <Grid container
                      direction="column">
                    {this.compileAttackFields()}

                    <Divider />

                    <Grid className={this.props.classes.characterAttacksField}
                          item
                          xs={12}>
                        <Button color="primary"
                                onClick={this.handleAttackAdded}>
                            Add Attack
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
        );
    }

    private sortAttacksByAttackBonus = (currentAttack: CharacterAttack, nextAttack: CharacterAttack): number => {
        const numberPattern = /\d+/;
        const currentAttackMatch = numberPattern.exec(currentAttack.attackBonus);
        const currentAttackSize = currentAttackMatch != null ? Number(currentAttackMatch) : 0;
        const nextAttackMatch = numberPattern.exec(nextAttack.attackBonus);
        const nextAttackSize = nextAttackMatch != null ? Number(nextAttackMatch) : 0;

        return currentAttackSize - nextAttackSize;
    }

    private mapAttacksToJsx = (attack: CharacterAttack): JSX.Element => {
        return (
            <CharacterAttackFieldComponent attack={attack}
                                           onAttackDeleted={this.handleAttackDeleted}
                                           onAttackUpdated={this.handleAttackUpdated} />
        );
    }

    private mapComponentToGridItem = (component: JSX.Element, index: number): JSX.Element => {
        return (
            <React.Fragment key={component.props.attack.id}>
                {index > 0 && <Divider />}
                <Grid item>
                    {component}
                </Grid>
            </React.Fragment>
        );
    }
}

const characterAttacksComponent = withStyles(characterAttackStyles)(CharacterAttacksComponent);

export {characterAttacksComponent as CharacterAttacksComponent};
