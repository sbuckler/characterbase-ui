import {Grid, IconButton, withStyles, WithStyles} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import * as React from "react";
import {CharacterAttack, DamageType} from "../../types/character-attack";
import {CharacterOptionFieldComponent} from "../CharacterOptionField/character-option.field.component";
import {CharacterTextFieldComponent} from "../CharacterTextField/character-text.field.component";
import {characterAttackStyles} from "./character-attacks.styles";

type Props = WithStyles<typeof characterAttackStyles> & {
    attack: CharacterAttack;
    onAttackUpdated: (attack: CharacterAttack) => void;
    onAttackDeleted: (attackId: string) => void;
};

type DamageTypeOption = {
    id: string;
    value: string;
};

class CharacterAttackFieldComponent extends React.Component<Props> {
    public readonly damageTypes: DamageTypeOption[];

    constructor(props: Props) {
        super(props);

        this.damageTypes = [
            {id: "bld", value: "Bludgeoning"},
            {id: "prc", value: "Piercing"},
            {id: "sls", value: "Slashing"}
        ];
    }

    public handleNameChanged = (updatedName: string): void => {
        this.props.onAttackUpdated({...this.props.attack, name: updatedName});
    }

    public handleAttackBonusChanged = (updatedAttackBonus: string): void => {
        this.props.onAttackUpdated({...this.props.attack, attackBonus: updatedAttackBonus});
    }

    public handleDamageChanged = (updatedDamage: string): void => {
        this.props.onAttackUpdated({...this.props.attack, damage: updatedDamage});
    }

    public handleDamageTypeSelected = (damageTypeId: string): void => {
        const selectedDamageType = this.getSelectedDamageType(damageTypeId);
        const updatedDamageType = {
            id: selectedDamageType.id,
            name: selectedDamageType.value
        } as DamageType;

        this.props.onAttackUpdated({...this.props.attack, damageType: updatedDamageType});
    }

    public handlePropertiesChanged = (updatedProperties: string): void => {
        this.props.onAttackUpdated({...this.props.attack, properties: updatedProperties});
    }

    public handleAttackDeleted = (): void => {
        this.props.onAttackDeleted(this.props.attack.id);
    }

    public render() {
        return (
            <Grid container
                  alignItems="baseline">
                <Grid className={this.props.classes.characterAttacksField}
                      item
                      xs={4}>
                    <CharacterTextFieldComponent currentValue={this.props.attack.name}
                                                 fullWidth={true}
                                                 label="Name"
                                                 onChangeHandler={this.handleNameChanged}
                                                 readOnly={false} />
                </Grid>

                <Grid className={this.props.classes.characterAttacksField}
                      item
                      xs={2}>
                    <CharacterTextFieldComponent currentValue={this.props.attack.attackBonus}
                                                 fullWidth={true}
                                                 label="Attack Bonus"
                                                 onChangeHandler={this.handleAttackBonusChanged}
                                                 readOnly={false} />
                </Grid>

                <Grid className={this.props.classes.characterAttacksField}
                      item
                      xs={3}>
                    <CharacterTextFieldComponent currentValue={this.props.attack.damage}
                                                 fullWidth={true}
                                                 label="Damage"
                                                 onChangeHandler={this.handleDamageChanged}
                                                 readOnly={false} />
                </Grid>

                <Grid className={this.props.classes.characterAttacksField}
                      item
                      xs={3}>
                    <CharacterOptionFieldComponent currentValue={this.props.attack.damageType.id}
                                                   fullWidth={true}
                                                   label="Damage Type"
                                                   onChangeHandler={this.handleDamageTypeSelected}
                                                   options={this.damageTypes}
                                                   readOnly={false} />
                </Grid>

                <Grid className={this.props.classes.characterAttacksField}
                      item
                      xs={11}>
                    <CharacterTextFieldComponent currentValue={this.props.attack.properties}
                                                 fullWidth={true}
                                                 label="Properties"
                                                 onChangeHandler={this.handlePropertiesChanged}
                                                 readOnly={false} />
                </Grid>

                <Grid className={this.props.classes.characterAttacksField}
                      item
                      xs={1}>
                    <IconButton color="primary"
                                onClick={this.handleAttackDeleted}>
                        <DeleteIcon />
                    </IconButton>
                </Grid>
            </Grid>
        );
    }

    private getSelectedDamageType = (damageTypeId: string): DamageTypeOption => {
        return this.damageTypes
            .filter((damageType: DamageTypeOption) => damageType.id === damageTypeId)[0];
    }
}

const characterAttackFieldComponent = withStyles(characterAttackStyles)(CharacterAttackFieldComponent);

export {characterAttackFieldComponent as CharacterAttackFieldComponent};
