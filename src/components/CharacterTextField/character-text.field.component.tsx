import {FormControl, TextField} from "@material-ui/core";
import * as React from "react";

type Props = {
    currentValue?: string | number;
    fullWidth?: boolean;
    label: string;
    max?: number;
    min?: number;
    onChangeHandler?: (value: string | number) => void;
    readOnly: boolean;
};

type State = {
    value: string | number;
};

export class CharacterTextFieldComponent extends React.Component<Props, State> {
    public state: State;

    constructor(props: Props) {
        super(props);

        this.state = {
            value: props.currentValue != null ? props.currentValue : ""
        };
    }

    public render(): JSX.Element {
        return (
            <FormControl fullWidth={this.props.fullWidth}>
                <TextField fullWidth={this.props.fullWidth}
                           InputProps={{
                               disableUnderline: this.props.readOnly,
                               readOnly: this.props.readOnly
                           }}
                           label={this.props.label}
                           onBlur={this.handleOnBlur}
                           onChange={this.handleOnChange}
                           value={this.state.value} />
            </FormControl>
        );
    }

    private handleOnChange = (event: React.FormEvent): void => {
        const eventTarget = event.target as HTMLSelectElement;

        this.setState(() => ({value: eventTarget.value}));
    }

    private handleOnBlur = (event: React.FormEvent): void => {
        const eventTarget = event.target as HTMLSelectElement;
        let updatedValue: string | number = this.getMaxValue(eventTarget.value);
        updatedValue = this.getMinValue(updatedValue);

        this.setState(() => ({value: updatedValue}));

        if (this.props.onChangeHandler == null) {
            return;
        }

        this.props.onChangeHandler(updatedValue);
    }

    private getMaxValue = (value: string | number): string | number => {
        if (this.props.max == null) {
            return value;
        }

        const minValue = isNaN(Number(value)) ? 0 : Number(value);

        return Math.min(minValue, this.props.max);
    }

    private getMinValue = (value: string | number): string | number => {
        if (this.props.min == null) {
            return value;
        }

        const maxValue = isNaN(Number(value)) ? 0 : Number(value);

        return Math.max(maxValue, this.props.min);
    }
}
