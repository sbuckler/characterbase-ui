import {createStyles} from "@material-ui/core";
import {StyleRules} from "@material-ui/core/styles";

export const numberStepperStyles = (): StyleRules => createStyles({
    stepper: {
        height: "50%",
        minHeight: "50%",
        padding: 0,
        position: "absolute",
        right: 0,
    },
    stepperDecrement: {
        bottom: 0
    },
    stepperIncrement: {
        top: 0
    }
});
